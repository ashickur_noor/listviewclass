package com.example.ashickurrahman.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.*;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter<PersonalInfo> arrayAdapter;
    ArrayList<PersonalInfo> arrayListPerson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listView=findViewById(R.id.listview);

        arrayListPerson=new ArrayList<>();

        arrayAdapter=new ArrayAdapter<>(this,R.layout.layout2,arrayListPerson);
        listView.setAdapter(arrayAdapter);

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(MainActivity.this, country.get(position), Toast.LENGTH_SHORT).show();
//
//
//
//            }
//        });
        addDataPerson();


    }

    private void addDataPerson() {
        arrayListPerson.add(new PersonalInfo("Noor","1223"));
        arrayListPerson.add(new PersonalInfo("Noor2","12234"));
    }
}
